import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Link from 'App/Models/Link'
import AddLinkValidator from 'App/Validators/AddLinkValidator'

export default class LinksController {
  public async index({ response, request }: HttpContextContract) {
    const { page = 1, limit = 10 } = request.qs()

    const data = await Link.query().paginate(page, limit)

    response.ok({
      message: "Berhasil mengambil data Links",
      data
    })
  }

  public async store({ request, response }: HttpContextContract) {
    const payload = await request.validate(AddLinkValidator)

    const data = await Link.create(payload)
    response.created({
      message: "Berhasil menyimpan link",
      data
    })
  }

  public async show({ }: HttpContextContract) { }

  public async update({ }: HttpContextContract) { }

  public async destroy({ }: HttpContextContract) { }
}
