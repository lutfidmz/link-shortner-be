import { schema, CustomMessages, rules } from '@ioc:Adonis/Core/Validator'
import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class AddLinkValidator {
  constructor(protected ctx: HttpContextContract) { }

  /*
   * Define schema to validate the "shape", "type", "formatting" and "integrity" of data.
   *
   * For example:
   * 1. The username must be of data type string. But then also, it should
   *    not contain special characters or numbers.
   *    ```
   *     schema.string({}, [ rules.alpha() ])
   *    ```
   *
   * 2. The email must be of data type string, formatted as a valid
   *    email. But also, not used by any other user.
   *    ```
   *     schema.string({}, [
   *       rules.email(),
   *       rules.unique({ table: 'users', column: 'email' }),
   *     ])
   *    ```
   */
  public schema = schema.create({
    title: schema.string.optional([rules.minLength(3)]),
    longUrl: schema.string([rules.url()]),
    shortUrl: schema.string.optional([
      rules.unique({ table: 'links', column: 'short_url', caseInsensitive: false }),
      rules.minLength(3)
    ]),
    password: schema.string.optional([rules.minLength(6)]),
    members: schema.array.optional().members(schema.string([rules.email()])),
    access: schema.enum.optional(['PUBLIC', 'INVITED']),
    expiredAt: schema.date.optional({ format: 'yyyy-mm-dd hh:MM:ss' }),
  })

  /**
   * Custom messages for validation failures. You can make use of dot notation `(.)`
   * for targeting nested fields and array expressions `(*)` for targeting all
   * children of an array. For example:
   *
   * {
   *   'profile.username.required': 'Username is required',
   *   'scores.*.number': 'Define scores as valid numbers'
   * }
   *
   */
  public messages: CustomMessages = {}
}
