import { DateTime } from 'luxon'
import { BaseModel, afterFetch, beforeCreate, beforeSave, column } from '@ioc:Adonis/Lucid/Orm'
import { v4 as uuid } from 'uuid'
import Hash from '@ioc:Adonis/Core/Hash'

export default class Link extends BaseModel {
  @column({ isPrimary: true })
  public id: string

  @column()
  public title: string | undefined

  @column()
  public longUrl: string

  @column()
  public shortUrl: string | undefined

  @column({ serializeAs: null })
  public password: string | undefined

  @column()
  public members: string[] | undefined

  @column()
  public access: string | undefined

  @column.dateTime()
  public expiredAt: DateTime | undefined


  @beforeCreate()
  public static newId(link: Link) {
    const newId = uuid()
    link.id = newId
  }


  @beforeSave()
  public static async hashPassword(link: Link) {
    if (link.$dirty.password) {
      link.password = await Hash.make(link.password!)
    }
  }

  @afterFetch()
  public static async getLinks(links: Link[]) {
    links.map(link => {
      link.shortUrl = 'https://localhost:3340/?p=' + link.shortUrl
    })
  }


  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
