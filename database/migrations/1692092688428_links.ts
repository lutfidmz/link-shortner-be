import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
  protected tableName = 'links'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.uuid('id').primary().notNullable().unique()
      table.string('title')
      table.string('long_url')
      table.string('short_url')
      table.string('password')
      table.specificType('members', 'varchar[]').comment('array of email')
      table.enum('access', ['PUBLIC', 'INVITED']).defaultTo('PUBLIC')
      table.timestamp('expired_at', { useTz: false })
      table.uuid('user_id').references('users.id')


      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamp('created_at', { useTz: true })
      table.timestamp('updated_at', { useTz: true })
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
